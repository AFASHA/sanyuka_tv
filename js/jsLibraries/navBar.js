$(document).ready(function() {
    var menu=document.querySelectorAll("li");
    var menuButton=document.querySelector("#menuButton")
    menuButton.addEventListener('click',function(){
        toggleMenu();
    },true)
    menu.forEach(element => {
        element.addEventListener('mouseover',function(){
            if(element.childNodes.length>0){
         subMenuToggleOn(element);
            }
        })
    });
    
    menu.forEach(element => {
        element.addEventListener('mouseout',function(){
            if(element.childNodes.length>0){
         subMenuToggleOff(element);
            }
        })
    });
    
});


function toggleMenu() {
    var x = document.querySelector('.nav-bar');
    console.log("change")
    if (x.style.display!="block") {
        x.style.display= "block";
        document.querySelector("#menuButton").children[0].className="fa fa-window-close"
        console.log("to display block")
    } else {
        x.style.display= "none";
        document.querySelector("#menuButton").children[0].className="fa fa-bars"
        console.log("to none")
    }

    console.log(x.style.display);
}


function setMenuHeader(me){

    var newName=$(me).text()
    console.log(newName)
     var menuName=document.getElementById("menuName");
     menuName.innerText=newName;
}

function navigation(me){
    setMenuHeader(me);
    toggleMenu();
}

function subMenuToggleOn(selector){
    var submenu=selector.childNodes;
    submenu.forEach(element => {
        if(element.tagName=="UL"){
          if(element.style.display!="block"){
                element.style.display="block"
            }
        }
    });
}

function subMenuToggleOff(selector){
    var submenu=selector.childNodes;
    submenu.forEach(element => {
        if(element.tagName=="UL"){
          if(element.style.display!="none"){
                element.style.display="none"
            }
        }
    });
}



